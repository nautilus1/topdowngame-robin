tool
extends EditorScript


func _run():
	configure_offset_tilemaps();


func configure_offset_tilemaps():
	var tile_set: TileSet = get_scene().get_node("TileMaps/Nodes").tile_set
	
	for tile_id in tile_set.get_tiles_ids():
		tile_set.tile_set_texture_offset(tile_id, Vector2(-tile_set.tile_get_region(tile_id).size.x/2, -tile_set.tile_get_region(tile_id).size.y/2));
		if tile_set.tile_get_shape(tile_id, 0):
			tile_set.tile_set_shape_offset(tile_id, 0, Vector2(-tile_set.tile_get_region(tile_id).size.x/2, -tile_set.tile_get_region(tile_id).size.y/2));
