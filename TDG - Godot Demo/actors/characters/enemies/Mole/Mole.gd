extends "res://actors/characters/Character.gd"

var player: KinematicBody2D;
var distance: float;


func _ready() -> void:
	set_process(false);
	
	state_machine = $StateMachine;
	animation_player = $Anim;
	
	state_machine.start();


func _process(delta):
	if player:
		if player.global_position.x > self.global_position.x:
			direction.x = 1;
		else:
			direction.x = -1;
		
		if player.global_position.y > self.global_position.y:
			direction.y = 1;
		else:
			direction.y = -1;
		
		distance = player.global_position.distance_to(self.global_position);


func _on_Perimeter_body_entered(body: KinematicBody2D) -> void:
	if body and body.is_in_group("Players"):
		player = body;
		distance = -body.global_position.distance_to(self.global_position);
		state_machine.change_state("Walk");
		set_process(true);
		

func _on_Perimeter_body_exited(body: KinematicBody2D) -> void:
	if body and body.is_in_group("Players") and player:
		player = null;
		distance = 0;
		direction = Vector2(0, 0);
		set_process(false);
