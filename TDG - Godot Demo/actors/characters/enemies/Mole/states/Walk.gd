extends "res://scripts/state_machine/default_states/run_default_state.gd"


func enter() -> void:
	pass


func update(delta) -> void:
	set_anim_direction();
	.update(delta);


func set_anim_direction() -> void:
	anim_character.play("WalkSide");


func exit() -> void:
	direction = Vector2(0, 0);
