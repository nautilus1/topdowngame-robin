extends "res://scripts/state_machine/default_states/idle_default_state.gd"


func enter() -> void:
	set_anim_direction();


func update(delta) -> void:
	.update(delta);


func set_anim_direction() -> void:
	if direction.x == -1 and direction.y == -1:
		anim_character.play("IdleBack");
	elif direction.x == 1 and direction.y == 1:
		anim_character.play("IdleFront");
	elif direction.x == -1 and direction.y == 1:
		anim_character.play("IdleSide");
	else:
		anim_character.play("IdleSide");

