extends "res://scripts/state_machine/default_states/run_default_state.gd"


func update(delta) -> void:
	set_anim_direction();
	.update(delta);


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		parent.change_state("AttackBow")


func set_anim_direction() -> void:
	if !direction.x and direction.y == -1:
		anim_character.play("WalkBack");
	elif !direction.x and direction.y == 1:
		anim_character.play("WalkFront");
	elif direction.x and !direction.y:
		anim_character.play("WalkSide");
