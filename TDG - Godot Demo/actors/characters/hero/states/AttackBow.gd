extends "res://scripts/state_machine/state.gd"

var arrow = load("res://actors/arrow/Arrow.tscn");


func enter() -> void:
	get_direction();
	anim_character.stop();


func update(delta):
	set_anim_direction();


func set_anim_direction():
	if anim_character.is_playing() == false:
		if !direction.x and direction.y == -1:
			anim_character.play("AttackBowBack");
		elif !direction.x and direction.y == 1:
			anim_character.play("AttackBowFront");
		elif direction.x and !direction.y:
			anim_character.play("AttackBowSide");
		
		yield(anim_character, "animation_finished");
		
		arrow();
		parent.change_state("Idle");


func arrow() -> void:
	var new_arrow = arrow.instance();
	owner.get_parent().add_child(new_arrow);
	new_arrow.global_position = character.get_node("pivot/Bow_pos").global_position;
	new_arrow.direction = direction;
