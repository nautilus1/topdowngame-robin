extends "res://scripts/state_machine/state.gd"


func enter():
	anim_character.play(self_anim_name);


func update(delta) -> void:
	get_direction()
	
	if direction.x || direction.y:
		parent.change_state("Walk");
