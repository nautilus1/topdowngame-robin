extends "res://scripts/state_machine/state.gd"


func enter() -> void:
	pass


func update(delta) -> void:
	get_direction();
	
	if !direction.x and !direction.y:
		parent.change_state("Idle");
	
	move(delta);
